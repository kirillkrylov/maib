define("ContactSectionV2", [], function() {
	return {
		entitySchemaName: "Contact",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[

			/**BUTTONS REGION */
			// https://academy.terrasoft.ru/docs/developer/elements_and_components/basic_interface_elements/knopka#case-3194
			{
				"operation": "insert",
				"name": "MyRedButton",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"values":{
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					},
					"itemType": this.Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					classes: {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					"caption": "SECTION COMB",
					"hint": {"bindTo": "Resources.Strings.MyRedBtnHint"},
					"click": {"bindTo": "onMyMainButtonClick"},
					tag: "ContactGeneralInfoBlock_Red"
				}
			},
			{
				"operation": "insert",
				"name": "MyGreenButton",
				"parentName": "ActionButtonsContainer",
				"propertyName": "items",
				"values":{
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					},
					"itemType": this.Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					classes: {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					"caption": "SECTION ABC",
					"click": {"bindTo": "onMyMainButtonClick"},
					tag: "ContactGeneralInfoBlock_Red",
					"menu":{
						"items": [
							{
								caption: "Sub Item 1",
								click: {bindTo: "onMySubButtonClick"},
								visible: true,
								hint: "Sub item 1 hint",
								tag: "subItem1"
							},
							{
								caption: "Sub Item 2",
								click: {bindTo: "onMySubButtonClick"},
								visible: true,
								hint: "Sub item 2 hint",
								tag: "subItem2"
							}
						]
					}
				}
			}
			/** END OF BUTTONS REGIONS */



		]/**SCHEMA_DIFF*/,
		methods: {

			onMyMainButtonClick: function(){
				//var id = this.get("GridData").get(this.$ActiveRow).$Id
				this.sandbox.publish("SectionActionClicked", "message body", [this.sandbox.id+"_CardModuleV2"])
			},

			getSectionActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({Type: "Terrasoft.MenuSeparator",Caption: ""}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": "Section Action Item One",
					"Click": {bindTo: "onActionClick"},
					"Enabled": true,
					"Tag": "Item1"
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": "Section Action Item Two",
					"Click": {bindTo: "onActionClick"},
					"Enabled": true,
					"Tag": "Item2",
				}));
				return actionMenuItems;
			},
			onActionClick: function(){
				let tag = arguments[0];
	
				//use tag to handle button clicks
				if(tag){
					this.showInformationDialog("Button with tag: "+tag+" clicked");
				}
			}
		},
		messages:{
			//Subscride on:  ContactPageV2.MaibFirstPackage
			"SectionActionClicked": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.PUBLISH
			}
		}
	};
});
