define("ContactPageV2", ["ServiceHelper", "MaibMixin"], function(ServiceHelper) {
	return {
		entitySchemaName: "Contact",
		attributes: {
			"MyEvents": {
				dependencies: [
					{
						columns: ["Email"],
						methodName: "onEmailChanged"
					},
					{
						columns: [ "Name"],
						methodName: "onNameChanged"
					}
				],
			},
			"Account": {
				lookupListConfig: {
					columns: ["Country", "Owner", "Web", "Country.Name", "Owner.Email"]
				}
			},
			"MyTransientDataColumn": {
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: this.Terrasoft.DataValueType.TEXT,
				caption: "My Transient Data Column",
				value: "This is my data"
			},

		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		
		mixins: {
			/**
			 * Provides methods for grid handling in ssp sections.
			 */
			"MaibMixin": "Terrasoft.MaibMixin"
		},
		methods: {
			/**
			 * @inheritdoc Terrasoft.BasePageV2#init
			 * @overridden
			 */
			init: function(){
				this.callParent(arguments);
				this.subscribeToMessages();
			},
			
			/**
			 * @inheritdoc Terrasoft.BasePageV2#onEntityInitialized
			 * @overridden
			 */
			onEntityInitialized: function() {
				this.callParent(arguments);
				this.GetDataFromExternalDatabase();
			},

			onNameChanged: function(){
				var colChangedName = arguments[1];
				var newValue = this.get(colChangedName);
				this.showInformationDialog("Value for column "+colChangedName + " is now set to: "+newValue);
			},
			
			onEmailChanged: function(){
				this.showInformationDialog("Value for column Email is now set to: "+this.$Email);
			},
			
			GetDataFromExternalDatabase: function() {
				//TODO: Call Configuration WebService(C#)
				this.$MyTransientDataColumn = "value from external web service"
			},


			/** ACTIONS REGION */
			/**
			 * @inheritdoc Terrasoft.BasePageV2#getActions
			 * @overridden
			 */
			 getActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({Type: "Terrasoft.MenuSeparator",Caption: ""}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": "Page Action Item One",
					"Click": {bindTo: "onActionClick"},
					"Enabled": true,
					"Tag": "Item1",
					ImageConfig: this.get("Resources.Images.CreatioSquare"),
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": "Page Action Item Two",
					"Click": {bindTo: "onActionClick"},
					"Enabled": true,
					"Tag": "Item2",
					"Items": {"bindTo": "addSubItems"}
				}));
				return actionMenuItems;
			},
			
			addSubItems: function() {
				var collection = this.Ext.create("Terrasoft.BaseViewModelCollection");
				collection.addItem(this.getButtonMenuItem({
					"Caption": {"bindTo": "Resources.Strings.SubItemOneCaption"},
					"Click": {"bindTo": "onActionClick"},
					"Tag": "sub1",
					ImageConfig: this.get("Resources.Images.CreatioSquare")
				}));
				collection.addItem(this.getButtonMenuItem({
					"Caption": {"bindTo": "Resources.Strings.SubItemTwoCaption"},
					"Click": {"bindTo": "onActionClick"},
					"Tag": "sub2",
					ImageConfig: this.get("Resources.Images.CreatioSquare")
				}));
				return collection;
			},

			onActionClick: function(){
				let tag = arguments[0];

				//use tag to handle button clicks
				if(tag){
					this.showInformationDialog("Button with tag: "+tag+" clicked");
				}
			},
			/** END OF ACTIONS REGION */

			/**BUTTONS REGION */
			/** END OF BUTTONS REGIONS */

			subscribeToMessages: function(){
				this.sandbox.subscribe(
					"SectionActionClicked",
					function(){this.onSectionMessageReceived();},
					this,
					[this.sandbox.id]
				)
			},

			onSectionMessageReceived: function(){
				var yB = this.Terrasoft.MessageBoxButtons.YES;
				yB.style = "GREEN";
				
				var nB = this.Terrasoft.MessageBoxButtons.NO;
				nB.style = "RED";

				this.showConfirmationDialog(
					"ARE YOU SURE YOU WANT TO PROCEED ?",
					function (returnCode) {
						if (returnCode === this.Terrasoft.MessageBoxButtons.NO.returnCode) {
							this.serviceExample();
						}					
						if (returnCode === this.Terrasoft.MessageBoxButtons.YES.returnCode) {
							//window.console.log("yes clicked");
							this.doESQ();
						}
					},
					[
						//this.Terrasoft.MessageBoxButtons.NO.returnCode,
						//this.Terrasoft.MessageBoxButtons.YES.returnCode
						yB.returnCode,
						nB.returnCode
					],
					null
				);
			},

			doESQ: function(){
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Account"});
				esq.addColumn("Id");
				esq.addColumn("Name");
				esq.addColumn("Industry");
				esq.addColumn("AlternativeName");

				var esqFirstFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Country.Name", "Афганистан");
				var esqSecondFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Country.Id", "e0be1264-f36b-1410-fa98-00155d043204");

				esq.filters.logicalOperation = Terrasoft.LogicalOperatorType.OR;
				esq.filters.add("esqFirstFilter", esqFirstFilter);
				esq.filters.add("esqSecondFilter", esqSecondFilter);

				esq.getEntityCollection(
					function (result) {
						if (!result.success) {
							// error processing/logging, for example
							this.showInformationDialog("Data query error");
							return;
						}
						result.collection.each(
							function (item) {
								i++;
								var name = name + " "+item.$Name;
						});
						this.showInformationDialog("Total Accounts: " + i);
					},
					this
				);
			},

			serviceExample: function(){
				var serviceData = {
					"person":{
						"MyName": "Kirill",
						"MyAge": 40,
						"Id": this.$Id
					}
				}
				// Calling the web service and processing the results.
				//https://[appName].domaincom/0/rest/ClassName/MethodName
				ServiceHelper.callService(
					"DemoWebService", 
					"PostMethodName",
					function(response) {
						debugger;
						this.showInformationDialog(response.PostMethodNameResult.MyAge);
					}, 
					serviceData, 
					this
				);
			}
		},
		messages:{
			//Subscride on:  ContactPageV2.MaibFirstPackage
			"SectionActionClicked": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "Name",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3
						//"layoutName": "ContactGeneralInfoBlock"
					},
					"bindTo": "Name"
				},
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "MyTransientDataColumn",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3
					},
					"bindTo": "MyTransientDataColumn",
					"visible" : true,
					"enabled": true
				},
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"index": 7
			},
			
			/**BUTTONS REGION */
			// https://academy.terrasoft.ru/docs/developer/elements_and_components/basic_interface_elements/knopka#case-3194
			{
				"operation": "insert",
				"name": "MyRedButton",
				"parentName": "LeftContainer",
				"propertyName": "items",
				"values":{
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					},
					"itemType": this.Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					classes: {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					"caption": {"bindTo": "Resources.Strings.MyRedBtnCaption"},
					"hint": {"bindTo": "Resources.Strings.MyRedBtnHint"},
					"click": {"bindTo": "onSectionMessageReceived"},
					tag: "ContactGeneralInfoBlock_Red"
				}
			},
			{
				"operation": "insert",
				"name": "MyGreenButton",
				"parentName": "LeftContainer",
				"propertyName": "items",
				"values":{
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					},
					"itemType": this.Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					classes: {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					"caption": "Green Button",
					"click": {"bindTo": "onMyMainButtonClick"},
					tag: "ContactGeneralInfoBlock_Red",
					"menu":{
						"items": [
							{
								caption: "Sub Item 1",
								click: {bindTo: "onMySubButtonClick"},
								visible: true,
								hint: "Sub item 1 hint",
								tag: "subItem1"
							},
							{
								caption: "Sub Item 2",
								click: {bindTo: "onMySubButtonClick"},
								visible: true,
								hint: "Sub item 2 hint",
								tag: "subItem2"
							}
						]
					}
				}
			},



			/** END OF BUTTONS REGIONS */


		]/**SCHEMA_DIFF*/
	};
});