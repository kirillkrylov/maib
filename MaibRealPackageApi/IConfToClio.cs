﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terrasoft.Core;

namespace MaibRealPackageApi
{
	public interface IConfToClio
	{
		void PostMessageToAll(string senderName, string messageText);
		void PostMessage(UserConnection userConnection, string senderName, string messageText);
		void LogInfo(string message);
		
		/// <summary>
		/// Get instance of a Logger
		/// </summary>
		/// <param name="name">Name of the logger as defined in nlog.config</param>
		/// <returns>Instance of a Logger</returns>
		ILog GetLogger(string name);

		/// <summary>
		/// Checks is feature enabled.
		/// </summary>
		/// <param name="userConnection">UserConnection</param>
		/// <param name="featureCode">Feature Code</param>
		/// <returns>Is feature enabled</returns>
		bool GetIsFeatureEnabled(UserConnection userConnection, string featureCode);
	}
}
