﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaibRealPackageApi
{
	public interface IMyBusinessLogic
	{
		decimal Add(decimal x, decimal y);
		decimal Sub(decimal x, decimal y);
	}
}
