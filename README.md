
# Введение в разработку на платформе Creatio c Кириллом Крыловым
С 24 Мая 2021 по 27 Мая 2021

## Видео запись уроков
|День|Запись|
|:--:|:--:|
|1|[Запись][d1v]|
|2|[Запись][d2v]|
|3|[Запись][d3v]|
|4|[Запись][d4v]| 

## **[Форма обратной связи][feedbackForm]**
Пожалуйста пройдите опрос удовлетворенности обучения по **[ссылке][feedbackForm]**


## Полезные материалы
- [Развертывание on-site][razvertyvanie_onsite]
- [Файловый контент пакетов][rabota_s_faylovym_kontentom]
- [Внешние IDE][vneshnie_ide]
- [Разработка исходного кода в файловом контенте (пакет-проект)][clio]
- [Custom Logging with NLog][nlog]
- [Элемент процесса Задание-сценарий][element_processa_zadanije_scenarij]
- [Бизнес-логика объектовEventListener_Contact][sobytiynyy_sloy_obekta]
- **[Настройка IdentityService][IdentityServiceYouTube]**

<!-- Named Links Video Recordings-->
[d1v]:https://creatio-global.zoom.us/rec/share/KksOiVTcQN_Zurz43C_LZzRxZzzzCwIGmjYpXwuajqd-_kUiN1fOuYx_mX0nUCv0.s02BT3ZbPHsUPcsm
[d2v]:https://creatio-global.zoom.us/rec/share/nLgg-Gq0Q3HTmXpAmhZ-ZULXPj6QUqCKXvvphpZks9ewAOa6AxctKgynK6VxXerO.G_gb_fdkxMZ_X2Vm
[d3v]:https://creatio-global.zoom.us/rec/share/h1gJrS6jbXVwc5u-0rXPRiFTufKH6oX7mXHCfUYf6VkKTh16SuILOiKJZYMuNH2e.RyK-o6aErF9yS9bW
[d4v]:https://creatio-global.zoom.us/rec/share/CNAOVPGb8fuiIp-XYAUFd1ebdWtTvCA2tloG_KWnCBKLHIIlK1uKJCOrQfBwiHqX.bpmfQ3e9SZh3lNKD



GitIgnore - https://raw.githubusercontent.com/github/gitignore/master/VisualStudio.gitignore


<!-- Named Links Academy Materials -->
[razvertyvanie_onsite]:https://academy.terrasoft.ru/docs/user/ustanovka_i_administrirovanie/razvertyvanie_onsite
[rabota_s_faylovym_kontentom]:https://academy.terrasoft.ru/docs/developer/development_tools/packages/rabota_s_faylovym_kontentom
[vneshnie_ide]:https://academy.terrasoft.ru/docs/developer/development_tools/development_in_external_ide/vneshnie_ide
[clio]: https://academy.terrasoft.ru/docs/developer/back-end_development/project_package/razrabotka_iskhodnogo_koda_v_faylovom_kontente_%28paket-proekt%29

[nlog]: https://github.com/Academy-Creatio/TrainingProgramm/wiki/Custom-Logging-with-NLog
[element_processa_zadanije_scenarij]: https://academy.terrasoft.ru/docs/user/biznes_processy/spravka_po_elementam_processov/dejstviya_sistemy/element_processa_zadanije_scenarij

[sobytiynyy_sloy_obekta]: https://academy.terrasoft.ru/docs/developer/back-end_development/entity_event_layer/sobytiynyy_sloy_obekta
[identityService]:https://academy.terrasoft.ru/docs/user/ustanovka_i_administrirovanie/razvertyvanie_onsite/nastrojka_dopolnitelnyh_parametrov_i_integracij/nastroit_avtorizaciyu_prilozhenij_po_protokolu_oauth20

[IdentityServiceYouTube]:https://youtu.be/ehjfcBxpLsQ


[feedbackForm]:https://forms.office.com/Pages/ResponsePage.aspx?id=-6Jce0OmhUOLOTaTQnDHFs1n4KjdfnVBtjvFqBN3Vk9UMTg5RkFPTjhBUkxUSzBZWlBOOTZZQU1DQi4u



## Уроки на YouTube

- [Enhlish Playlist][enuPlaylist]
- [Русский Playlist][rusPlaylist]

[enuPlaylist]:https://www.youtube.com/playlist?list=PLnolcTT5TeE3v8WGd3VqlZSd2D02GWSGa
[rusPlaylist]:https://www.youtube.com/playlist?list=PLDp-M9ZGnvgG4UL5gaeQUZDvDamePOumb