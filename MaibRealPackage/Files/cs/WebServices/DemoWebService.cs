﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Web.Common;

namespace MaibRealPackage
{
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class DemoWebService : BaseService
	{
		#region Properties
		private SystemUserConnection _systemUserConnection;
		private SystemUserConnection SystemUserConnection
		{
			get
			{
				return _systemUserConnection ?? (_systemUserConnection = (SystemUserConnection)AppConnection.SystemUserConnection);
			}
		}
		#endregion

		#region Methods : REST
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, 
			BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
		public DemoDTO PostMethodName(DemoDTO person)
		{
			//http://[appAddress]:port/0/rest/DemoWebService/PostMethodName

			UserConnection userConnection = UserConnection ?? SystemUserConnection;
			Select s = new Select(userConnection)
				.Column("Age")
				.From("Contact")
				.Where("Id").IsEqual(Column.Parameter(person.Id)) as Select;
			

			return new DemoDTO { Name = person.Name, Age = s.ExecuteScalar<int>() + 10 };
		}
		#endregion

		#region Methods : Private

		#endregion
	}


	[DataContract]
	public class DemoDTO
	{
		[DataMember(Name="MyName")]
		public string Name { get; set; }

		[DataMember(Name="MyAge")]
		public int Age { get; set; }

		[DataMember(Name="Id")]
		public Guid Id { get; set; }
	}
}