﻿using MaibRealPackageApi;
using Terrasoft.Core.Factories;

namespace MaibRealPackage
{

	[DefaultBinding(typeof(IMyBusinessLogic),Name = "MyFirstImplementation")]
	public class MyBusinessLogic : IMyBusinessLogic
	{
		public decimal Add(decimal x, decimal y)
		{
			var confToClio = ClassFactory.Get<IConfToClio>();
			var result = x + y;
			confToClio.PostMessageToAll(GetType().Name, result.ToString());
			return result;
		}

		public decimal Sub(decimal x, decimal y)
		{
			var confToClio = ClassFactory.Get<IConfToClio>();
			var result = x - y;
			confToClio.PostMessageToAll(GetType().Name, result.ToString());
			return result;
		}
	}

	[DefaultBinding(typeof(IMyBusinessLogic), Name = "MySecondImplementation")]
	public class MyBusinessLogic2 : IMyBusinessLogic
	{
		public decimal Add(decimal x, decimal y)
		{
			return x + y +200;
		}

		public decimal Sub(decimal x, decimal y)
		{
			return x - y -200;
		}
	}
}
