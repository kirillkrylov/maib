using Common.Logging;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Configuration;
using MaibRealPackageApi;
using Terrasoft.Core.Factories;

namespace MainRealPackage
{

	[DefaultBinding(typeof(IConfToClio))]
	public class ConfToClio: IConfToClio
	{
		public void PostMessageToAll(string senderName, string messageText)
		{
			MsgChannelUtilities.PostMessageToAll(senderName, messageText);
		}
		
		public void PostMessage(UserConnection userConnection, string senderName, string messageText)
		{
			MsgChannelUtilities.PostMessage(userConnection, senderName, messageText);
		}


		public ILog GetLogger(string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentNullOrEmptyException(nameof(name));
			}
			return LogManager.GetLogger(name);
		}

		public void LogInfo(string message)
		{
			var logger = LogManager.GetLogger("GuidedLearningLogger");
			logger.Info(message);
		}
		public bool GetIsFeatureEnabled(UserConnection userConnection, string featureCode)
		{
			return userConnection.GetIsFeatureEnabled(featureCode);
		}
	}
}