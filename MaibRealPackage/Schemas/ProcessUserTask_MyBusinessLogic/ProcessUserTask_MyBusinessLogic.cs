namespace Terrasoft.Core.Process.Configuration
{
	using MaibRealPackageApi;
	using Newtonsoft.Json;
	using Newtonsoft.Json.Linq;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Globalization;
	using Terrasoft.Common;
	using Terrasoft.Core;
	using Terrasoft.Core.Configuration;
	using Terrasoft.Core.DB;
	using Terrasoft.Core.Entities;
	using Terrasoft.Core.Process;
	using Terrasoft.UI.WebControls.Controls;

	#region Class: ProcessUserTask_MyBusinessLogic

	/// <exclude/>
	public partial class ProcessUserTask_MyBusinessLogic
	{

		#region Methods: Protected

		protected override bool InternalExecute(ProcessExecutingContext context) {
			var bl = Factories.ClassFactory.Get<IMyBusinessLogic>(impTag);
			switch (method)
			{
				case "add":
					result = bl.Add(X, Y);
					break;
				case "sub":
					result = bl.Sub(X, Y);
					break;
				default:
					break;
			}
			return true;
		}

		#endregion

		#region Methods: Public

		public override bool CompleteExecuting(params object[] parameters) {
			return base.CompleteExecuting(parameters);
		}

		public override void CancelExecuting(params object[] parameters) {
			base.CancelExecuting(parameters);
		}

		public override string GetExecutionData() {
			return string.Empty;
		}

		public override ProcessElementNotification GetNotificationData() {
			return base.GetNotificationData();
		}

		#endregion

	}

	#endregion

}

